<?php declare (strict_types=1);
    namespace GlintFMW\Tests\Routing;

    use GlintFMW\Dependencies\Injector;
    use GlintFMW\HTTP\Configuration\HTTPProvider;
    use GlintFMW\HTTP\Headers;
    use GlintFMW\HTTP\Parameters;
    use GlintFMW\HTTP\Request;
    use GlintFMW\HTTP\RequestFactory;
    use GlintFMW\Routing\Configuration\RoutingProvider;
    use GlintFMW\Routing\Dispatcher;
    use GlintFMW\Routing\Middleware;
    use PHPUnit\Framework\TestCase;

    class TestController
    {
        const NOTFOUND_RESULT = "notfound";
        const EXCEPTION_RESULT = "exception";
        const ROUTE_RESULT = "route";

        function notfound (string $path)
        {
            return self::NOTFOUND_RESULT;
        }

        function exception (\Throwable $exception)
        {
            return self::EXCEPTION_RESULT;
        }

        function route ()
        {
            return self::ROUTE_RESULT;
        }

        function routeParameter (string $parameter)
        {
            return $parameter;
        }

        function routeParameter2 (string $username = "default", Request $request)
        {
            return $username;
        }

        function __invoke (string $parameter)
        {
            return $parameter;
        }

        function throwException ()
        {
            throw new \Exception ();
        }
    };

    class ExampleMiddleware extends Middleware
    {
        public static $value = 0;

        function process(Request $request): void
        {
            self::$value = 1;
        }
    };

    class DispatcherTest extends TestCase
    {
        const ROUTING_CONFIGURATION = array (
            "routes" => array (
                "/" => "route@" . TestController::class,
                "/:parameter" => "routeParameter@" . TestController::class,
                "/example/test/:?username" => "routeParameter2@" . TestController::class,
                "/second/:parameter" => TestController::class,
                "/exception/example" => "throwException@" . TestController::class
            ),
            "notfound" => "notfound@" . TestController::class,
            "exception" => "exception@" . TestController::class,
            "middlewares" => array (
                ExampleMiddleware::class
            )
        );

        private RoutingProvider $configurationProvider;
        private Injector $dependencyInjector;

        function setUp (): void
        {
            // stub the http provider
            $httpProvider = $this->createStub (HTTPProvider::class);
            $httpProvider
                ->method ('getContentHandlers')
                ->willReturn (array ());

            // stub the RequestFactory
            $stub = $this->createStub (RequestFactory::class);
            $stub
                ->method ('makeRequestFromServerGlobal')
                ->willReturn (
                    new Request (
                        "GET", "/",
                        new Parameters (), new Headers (), new Parameters (),
                        "", $httpProvider
                    )
                );

            // setup dependency injection first
            $this->dependencyInjector = new Injector ();
            // register dependency for request factory and http provider
            $this->dependencyInjector->registerDependency ($stub);
            // mock request factory and register it as dependency
            // setup custom configuration provider for the routing system
            $this->configurationProvider = new RoutingProvider (
                RoutingProvider::convert (self::ROUTING_CONFIGURATION),
                $this->dependencyInjector
            );
            // ensure the configuration provider is registered in the dependency injection
            $this->dependencyInjector->registerDependency ($this->configurationProvider);
        }

        /**
         * @depends testConfiguration
         */
        function testRoutes ()
        {
            $routes = $this->configurationProvider->getRoutes ();
            // first route
            $this->assertCount (0, $routes [0]->getParameters ());
            $this->assertEquals ("^\\/$", $routes [0]->getRegex ());
            $this->assertEquals ("/", $routes [0]->getRoute ());
            $this->assertEquals (TestController::class, $routes [0]->getHandler ()->getClass ());
            $this->assertEquals ("route", $routes [0]->getHandler ()->getMethod ());
            // second route
            $this->assertCount (1, $routes [1]->getParameters ());
            $this->assertContains ("parameter", $routes [1]->getParameters ());
            $this->assertEquals ("^\\/([a-zA-Z0-9_%-]+)\\/$", $routes [1]->getRegex ());
            $this->assertEquals ("/:parameter/", $routes [1]->getRoute ());
            $this->assertEquals (TestController::class, $routes [1]->getHandler ()->getClass ());
            $this->assertEquals ("routeParameter", $routes [1]->getHandler ()->getMethod ());
            // third route
            $this->assertCount (1, $routes [2]->getParameters ());
            $this->assertContains ("username", $routes [2]->getParameters ());
            $this->assertEquals ("^\\/example\\/test\\/([a-zA-Z0-9_%-]+)?\\/?$", $routes [2]->getRegex ());
            $this->assertEquals ("/example/test/:?username/", $routes [2]->getRoute ());
            $this->assertEquals (TestController::class, $routes [2]->getHandler ()->getClass ());
            $this->assertEquals ("routeParameter2", $routes [2]->getHandler ()->getMethod ());
        }

        /** @depends testConfiguration */
        function testDispatching ()
        {
            // dispatch to final
            /** @var Dispatcher $dispatcher */
            $dispatcher = $this->dependencyInjector->inject (Dispatcher::class);
            $this->assertEquals (TestController::ROUTE_RESULT, $dispatcher->dispatch ("/"));
            $this->assertEquals ("test", $dispatcher->dispatch ("/test"));
            $this->assertEquals ("almamu", $dispatcher->dispatch ("/example/test/almamu"));
            $this->assertEquals ("result", $dispatcher->dispatch ("/second/result"));
            $this->assertEquals (TestController::NOTFOUND_RESULT, $dispatcher->dispatch ("/notfound/path"));
            $this->assertEquals (TestController::EXCEPTION_RESULT, $dispatcher->dispatch ("/exception/example"));

            // ensure the middleware was ran
            $this->assertEquals (1, ExampleMiddleware::$value);
        }

        function testConfiguration ()
        {
            $this->assertCount (5, $this->configurationProvider->getRoutes ());
        }
    }