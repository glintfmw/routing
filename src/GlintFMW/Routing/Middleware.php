<?php declare (strict_types=1);
    namespace GlintFMW\Routing;

    use GlintFMW\Dependencies\Injector;
    use GlintFMW\HTTP\Request;

    /**
     * Base class for routing middlewares
     *
     * @package GlintFMW\Routing
     * @author Alexis Maiquez <almamu@almamu.com>
     */
    abstract class Middleware
    {
        /** @var Injector The dependency injector to use in this class */
        private Injector $dependencyInjector;

        function __construct (Injector $dependencyInjector)
        {
            $this->dependencyInjector = $dependencyInjector;
        }

        /** @return Injector The dependency injector */
        protected function getDependencyInjector ()
        {
            return $this->dependencyInjector;
        }

        /**
         * @param Request $request The HTTP request object
         */
        abstract function process (Request $request): void;
    }