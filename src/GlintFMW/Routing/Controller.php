<?php declare (strict_types=1);
    namespace GlintFMW\Routing;

    /**
     * Base class for any controller of the system
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Routing
     */
    abstract class Controller
    {
    };