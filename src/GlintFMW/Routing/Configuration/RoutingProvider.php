<?php declare (strict_types=1);
	/**
	 * @author Almamu
	 */
	namespace GlintFMW\Routing\Configuration;

	use GlintFMW\Configuration\Exception\IntegrityException;
    use GlintFMW\Configuration\Providers\Map;
    use GlintFMW\HTTP\Request;
    use GlintFMW\Routing\Middleware;
    use GlintFMW\Routing\Route;
    use GlintFMW\Resources\URL;
    use ReflectionClass;
    use ReflectionFunction;

    /**
	 * Configuration provider for routing system
	 *
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Routing\Configuration
	 */
	class RoutingProvider extends Map
	{
		/** @inheritdoc */
		static function provides (): string
		{
			return "appRouting";
		}

        /**
         * Converts the given route string to a regular expression
         *
         * @param string $path The path to convert
         *
         * @return string The converted string to regular expression
         */
        private static function routeToRegExp (string $path)
        {
            $path = preg_quote ($path, '/');

            $regex = preg_replace ('/\\\\\\:\\\\\\?[a-zA-Z0-9_-]+\\\\\\//', '([a-zA-Z0-9_%-]+)?\\/?', $path);

            if (is_null ($regex) === true)
                throw new \Exception ("Cannot convert route to it's regular expression representation");

            return "^" . preg_replace ('/\\\\:[a-zA-Z0-9_-]+\\\\\\//', '([a-zA-Z0-9_%-]+)\\/', $regex) . "$";
        }

        /**
         * Extracts a list of parameters the URL has
         *
         * @param string $path The path to extract variable names from
         *
         * @return array<string> The list of parameters
         */
        private static function extractParameterNames (string $path): array
        {
            $matches = array ();

            preg_match_all ('/:\\??([a-zA-Z0-9_-]+)/', $path, $matches);

            // ignore the first element as it's not important
            array_shift ($matches);

            return array_shift ($matches);
        }

        /**
         * Extracts a list of parameters the URL has
         *
         * @param string $path The path to extract variable names from
         *
         * @return array<string> The list of parameters
         */
        private static function extractParameterNamesWithIndicator (string $path): array
        {
            $matches = array ();

            preg_match_all ('/:(\\??[a-zA-Z0-9_-]+)/', $path, $matches);

            // ignore the first element as it's not important
            array_shift ($matches);

            return array_shift ($matches);
        }


		/**
         * Performs checks on the configuration and produces the actual settings
         * to be used on runtime
         *
         * @param array<int|string, mixed> $input The configuration data available on the JSON
         * @return array{routes:array<Route>,notfound:Route,exception:Route}
         *
         * @throws \GlintFMW\Configuration\Exception\IntegrityException When the configuration is not valid
         */
		static function convert (array $input): array
		{
		    $routesList = array ();
		    $middlewares = array ();

		    if (array_key_exists ('routes', $input) === false)
                $input ['routes'] = array ();
		    if (array_key_exists ('notfound', $input) === false)
                throw new IntegrityException ('"notfound" default route not specified. Please add "notfound" to the appRouting section');
		    if (array_key_exists ('exception', $input) === false)
                throw new IntegrityException ('"exception" default route not specified. Please add "exception" to the appRouting section');
		    if (is_array ($input ['routes']) === false)
                throw new IntegrityException ('Routes must be a dictionary of routes');

            foreach ($input ['routes'] as $path => $controller)
            {
                $path = '/' . trim ($path, '/');

                if ($path !== '/')
                    $path .= '/';

                try
                {
                    $callback = URL::parseFunctionURL ($controller);
                    $method = $callback->getReflection ();

                    $parameters = self::extractParameterNames ($path);
                    $parametersWithIndicator = self::extractParameterNamesWithIndicator ($path);

                    if ($callback->isClass () === true)
                    {
                        // callable classes are a bit special
                        // all the parameters come in an array
                        // with the request object at the end
                        // so no parameter validation is needed
                        if ($method->getNumberOfParameters () !== 1)
                            throw new IntegrityException ("The parameter count of the callable class {$callback} must be 1");
                    }
                    else
                    {
                        if ($method->getNumberOfParameters () !== count ($parametersWithIndicator))
                        {
                            $difference = $method->getNumberOfParameters () - count ($parametersWithIndicator);

                            if ($difference == 1)
                            {
                                // when the difference is one this might be correct
                                // there are situations where the programmer might want
                                // to have the "Request" object
                                $request = 0;

                                foreach ($method->getParameters () as $index => $parameter)
                                {
                                    if (in_array ($parameter->getName (), $parameters, true) === false)
                                    {
                                        if ($request == 1)
                                            throw new IntegrityException ("Only one {Request::class} parameter is allowed");
                                        elseif (is_null ($parameter->getClass ()) === true || $parameter->getClass ()->getName () !== Request::class)
                                            throw new IntegrityException ("Parameter {$parameter->getName ()} is not present on {$callback}");

                                        $request ++;
                                    }
                                    if (in_array ('?' . $parameter->getName (), $parametersWithIndicator, true) === true && $parameter->isDefaultValueAvailable () === false)
                                        throw new IntegrityException ("Parameter {$index} must have a default value on {$callback}");
                                }
                            }
                            else
                                throw new IntegrityException (
                                    "{$callback} should only have " . count ($parametersWithIndicator) . " parameters"
                                );
                        }
                        else
                        {
                            // ensure the parameters are correct
                            foreach ($method->getParameters () as $index => $parameter)
                            {
                                if (in_array ($parameter->getName (), $parameters, true) === false)
                                    throw new IntegrityException (
                                        "Parameter {$parameter->getName ()} is not present on {$callback}"
                                    );
                                if (strpos ($parametersWithIndicator [$index], '?') === 0 && $parameter->isDefaultValueAvailable () === false)
                                    throw new IntegrityException ("Parameter {$index} must have a default value on {$callback}");
                            }
                        }
                    }

                    $route = new Route ();
                    $route
                        ->setHandler ($callback)
                        ->setRegex (self::routeToRegExp ($path))
                        ->setRoute ($path)
                        ->setParameters ($parameters);


                    $routesList [] = $route;
                }
                catch (\Exception $ex)
                {
                    throw new IntegrityException ("Error parsing path {$path}", 0, $ex);
                }
            }

            $notfound = new Route ();
            $exception = new Route ();

            // ensure notfound and exception have the requried parameters
            // so they can be used in the routing system
            $notfoundURL = URL::parseFunctionURL ($input ['notfound']);
            $exceptionURL = URL::parseFunctionURL ($input ['exception']);
            $notfoundReflection = $notfoundURL->getReflection ();
            $exceptionReflection = $exceptionURL->getReflection ();

            if (
                $notfoundReflection->getNumberOfParameters() != 1 ||
                $notfoundReflection->getParameters () [0]->getName () != "path" ||
                $notfoundReflection->getParameters () [0]->getType () != "string")
                throw new IntegrityException ("Not found handler must have one parameter named 'path' of type 'string'");
            if (
                $exceptionReflection->getNumberOfParameters () != 1 ||
                $exceptionReflection->getParameters () [0]->getName () != 'exception' ||
                $exceptionReflection->getParameters () [0]->getType () != \Throwable::class)
                throw new IntegrityException ("Exception handler must have one parameter named 'exception' of type {Throwable::class}");

            $notfound->setHandler ($notfoundURL);
            $exception->setHandler ($exceptionURL);

            // process middlewares if available
            if (array_key_exists ('middleware', $input) == true || array_key_exists ('middlewares', $input) == true)
            {
                $middlewares = $input ['middleware'] ?? $input ['middlewares'];

                foreach ($middlewares as $middleware)
                {
                    if (class_exists ($middleware) == false)
                        throw new IntegrityException ("Specificed an unknown middleware {$middleware}");
                    if (is_subclass_of ($middleware, Middleware::class, true) == false)
                        throw new IntegrityException ("Middleware must extend off the " . Middleware::class . " class");
                }
            }

            return array (
                'routes' => $routesList,
                'notfound' => $notfound,
                'exception' => $exception,
                'middleware' => $middlewares
            );
		}

        /** @return Route[] List of routes available */
		function getRoutes (): array
        {
            return $this->getConfiguration () ['routes'];
        }

        function getNotFoundHandler (): Route
        {
            return $this->getConfiguration () ['notfound'];
        }

        function getExceptionHandler (): Route
        {
            return $this->getConfiguration () ['exception'];
        }

        /** @return class-string[] List of middlewares */
        function getMiddlewares (): array
        {
            return $this->getConfiguration () ['middleware'];
        }
	};