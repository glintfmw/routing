<?php declare (strict_types=1);
	namespace GlintFMW\Routing\Exception;
    /**
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Routing\Exception
     */
	class ControllerNotFoundException extends \Exception
    {
        /** @var string The path requested */
        private string $path = "";

        function __construct (string $path)
        {
            parent::__construct ("Controller for path {$path} cannot be found");

            $this->path = $path;
        }

        /** @return string The path that the controller belongs to */
        function getPath (): string
        {
            return $this->path;
        }
    };