<?php declare (strict_types=1);
	namespace GlintFMW\Routing;

    use GlintFMW\Dependencies\Injector;
    use GlintFMW\HTTP\Parameters;
    use GlintFMW\HTTP\Request;
    use GlintFMW\HTTP\RequestFactory;
    use GlintFMW\Routing\Configuration\RoutingProvider;
    
    use GlintFMW\Routing\Exception\ControllerNotFoundException;

	/**
	 * Basic class that takes care of dispatching request
	 * to the correct controller
	 *
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Routing
	 */
	class Dispatcher
	{
	    private RoutingProvider $routingProvider;
	    private RequestFactory $requestFactory;
	    private Injector $dependencyInjector;

	    function __construct (RoutingProvider $routingProvider, RequestFactory $requestFactory, Injector $dependencyInjector)
        {
            $this->routingProvider = $routingProvider;
            $this->requestFactory = $requestFactory;
            $this->dependencyInjector = $dependencyInjector;
        }

        /**
         * Resolves the given path and returns the corresponding route
         *
         * @param string $path The path to resolve
         * @param mixed $matches Where to store the matches found
         * @return Route
         * @throws ControllerNotFoundException
         */
        function resolveRequestPath (string $path, &$matches): Route
        {
            // normalize path to properly parse it
            $path = '/' . trim ($path, '/');

            if ($path !== '/')
                $path .= '/';

            foreach ($this->routingProvider->getRoutes () as $route)
            {
                if (preg_match ('/' . $route->getRegex () . '/i', $path, $matches) === 1)
                {
                    return $route;
                }
            }

            throw new ControllerNotFoundException ($path);
        }

        /**
         * Handles the given path and returns the matching controller (if any)
         *
         * @param string $path The path to resolve
         * @param Route|null $route The route to dispatch the call to
         * @param array <string, mixed> $parameters The list of parameters
         * @param Request|null $request The request information to pass onto the controller (if required)
         *
         * @return mixed The response from the controller
         *
         * @throws \GlintFMW\Core\Dependencies\Exceptions\CircularDependencyException
         * @throws \GlintFMW\Dependencies\Exceptions\CacheNotBuiltException
         * @throws \GlintFMW\Dependencies\Exceptions\DependencyNotRegisteredException
         * @throws \GlintFMW\Types\Exceptions\ClassTypeDoesNotMatchException
         * @throws \ReflectionException
         */
        function dispatch (string $path, ?Route $route = null, ?array $parameters = null, ?Request $request = null)
        {
            try
            {
                if (is_null ($route) === false && is_null ($parameters) === true)
                    throw new \Exception ('Route and parameter have to be specified together');
                if (is_null ($route) === true)
                    $route = $this->resolveRequestPath ($path, $matches);

                if (is_null ($parameters) === true && isset ($matches) == true)
                {
                    // TODO: TAKE INTO ACCOUNT DIFFERENT NUMBER OF PARAMETERS
                    array_shift ($matches);

                    /** @var array<string, mixed> */
                    $parameters = array_combine ($route->getParameters (), $matches);
                }

                $parameters ??= array ();

                if (is_null ($request) === true)
                    $request = $this->requestFactory->makeRequestFromServerGlobal (
                        Parameters::fromArray ($parameters)
                    );

                $handler = $route->getHandler ();
                $reflection = $handler->getReflection ();

                // add the "Request" object if requested
                foreach ($reflection->getParameters () as $parameter)
                {
                    if (is_null ($parameter->getType ()) === true)
                        continue;

                    if ($parameter->getType () == Request::class)
                    {
                        if (array_key_exists ('request', $parameters) === true)
                            throw new \Exception ("Request parameter is already in the URL, cannot add to callable class");

                        $parameters [$parameter->getName ()] = $request;
                    }
                }

                // run all the required middlewares
                $middlewares = $this->routingProvider->getMiddlewares ();

                foreach ($middlewares as $middleware)
                {
                    /** @var class-string $middleware */
                    /** @var Middleware $object */
                    $object = new $middleware ($this->dependencyInjector);
                    $object->process ($request);
                }

                $controller = null;

                if ($handler->isFunction () == false)
                    // @phpstan-ignore-next-line
                    $controller = $this->dependencyInjector->inject ($handler->getClass ());

                return $handler->call ($controller, $parameters);
            }
            catch (ControllerNotFoundException $exception)
            {
                $handler = $this->routingProvider->getNotFoundHandler ()->getHandler ();
                $controller = null;

                if ($handler->isFunction () == false)
                    // @phpstan-ignore-next-line
                    $controller = $this->dependencyInjector->inject ($handler->getClass ());

                // call the not-found handler
                return $handler->call ($controller, array ('path' => $path));
            }
            catch (\Throwable $exception)
            {
                $handler = $this->routingProvider->getExceptionHandler ()->getHandler ();
                $controller = null;

                if ($handler->isFunction () == false)
                    // @phpstan-ignore-next-line
                    $controller = $this->dependencyInjector->inject ($handler->getClass ());

                // call the not-found handler
                return $handler->call ($controller, array ('exception' => $exception));
            }
        }
	};