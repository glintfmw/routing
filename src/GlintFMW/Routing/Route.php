<?php declare (strict_types=1);
    namespace GlintFMW\Routing;

    use GlintFMW\Types\Closure;

    /**
     * Represents a route in the routing module
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Routing
     */
    class Route
    {
        /** @var Closure Callable of function/method to call */
        private Closure $handler;
        /** @var string The actual regex used to parse the URL when dispatching */
        private string $regex;
        /** @var string The original route used in the configuration (for debug purposes) */
        private string $route;
        /** @var array<string> List of all the parameters the route has */
        private array $parameters;
        /** @var Middleware[] List of middlewares to run before this route is processed */
        private array $middlewares;

        /**
         * @param array<string,mixed> $input
         * @return Route
         */
        public static function __set_state (array $input)
        {
            $route = new Route ();

            $route
                ->setHandler     ($input ['handler'])
                ->setRegex       ($input ['regex'] ?? '')
                ->setRoute       ($input ['route'] ?? '')
                ->setParameters  ($input ['parameters'] ?? array ());

            return $route;
        }

        /** @return Closure */
        public function getHandler (): Closure
        {
            return $this->handler;
        }

        /**
         * @param Closure $handler
         * @return Route
         */
        public function setHandler (Closure $handler): self
        {
            $this->handler = $handler;

            return $this;
        }

        /** @return string */
        public function getRegex (): string
        {
            return $this->regex;
        }

        /**
         * @param string $regex
         * @return Route
         */
        public function setRegex (string $regex): self
        {
            $this->regex = $regex;

            return $this;
        }

        /** @return string */
        public function getRoute (): string
        {
            return $this->route;
        }

        /**
         * @param string $route
         * @return Route
         */
        public function setRoute (string $route): self
        {
            $this->route = $route;

            return $this;
        }

        /** @return array<string> */
        public function getParameters (): array
        {
            return $this->parameters;
        }

        /**
         * @param array<string> $parameters
         *
         * @return self
         */
        public function setParameters (array $parameters): self
        {
            $this->parameters = $parameters;

            return $this;
        }
    };